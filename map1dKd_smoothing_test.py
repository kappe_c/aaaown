from math import pi as PI

import numpy as np

import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt

from smooth_curves import smooth_curves


numSamples = 180
scalex = PI / 30 # the denominator should be half of the desired wavelength
bandwidth = 5


def main():
  # pure
  vals = np.linspace(0., numSamples * scalex, numSamples, endpoint=False)
  np.sin(vals, vals)
  #plt.plot(vals)
  
  # Notice that this is not about reconstructing the original signal.
  # The sine is just an example base function.
  # With a large enough bandwidth one can see the effect of the smoothing even on a pure sine
  # because the extrema would be considered too "pointy".
  
  # noisy
  vals += np.random.rand(numSamples) * 0.4 - 0.2
  plt.plot(vals)
  
  # smoothed
  plt.plot(smooth_curves(vals, None, bandwidth), linewidth=2)
  
  plt.show()


main()
