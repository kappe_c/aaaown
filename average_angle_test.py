"""
This is not a quantitative test because there is no simple alternative to test against.
Instead, this module just creates a visual representation of a random set of angles and their
average to see whether the code runs without crash and the result is as expected.
The commandline output also includes the Euclidean norm of the Cartesian mean of the angles
interpreted as points on the unit circle.
"""
from math import pi as PI, sin, cos, atan2, sqrt
import turtle

import numpy as np

from average_angle import average_angle


DEG2RAD = PI / 180.
RAD2DEG = 180. / PI
TWOPI = PI * 2.

#--------------------------------------------------------------------------------------------------#

def angles_to_cartesian_points(angles):
  pts = np.empty((len(angles), 2))
  for i, angle in enumerate(angles):
    pts[i] = cos(angle), sin(angle)
  return pts

#--------------------------------------------------------------------------------------------------#

def run():
  num = np.random.randint(2, 13)
  #anglesInDeg = np.random.randint(0, 360, num)
  anglesInDeg = np.random.randint(0, 270, num) # a little more realistic
  print("Input angles (%d):" % num, anglesInDeg)
  angles = anglesInDeg * DEG2RAD # creates new float from int
  
  angularAverage = average_angle(angles) % TWOPI # result is in no specific range
  
  points = angles_to_cartesian_points(angles)
  mx, my = points.mean(0)
  cartesianAverage = atan2(my, mx) % TWOPI # Python atan2 is in (-pi, pi]
  unison = sqrt(mx*mx + my*my)
  d = abs(cartesianAverage - angularAverage)
  if d > PI: # is this even possible?
    d = TWOPI - d
  
  print("Unison (blue distance from center): %.1f %%" % (unison * 100))
  print("Angular   average  (green): %.1f" % (angularAverage * RAD2DEG))
  print("Cartesian average (orange): %.1f" % (cartesianAverage * RAD2DEG))
  print("Absolute difference: %.1f" % (d * RAD2DEG))
  
  
  turtle.setup(300, 300, -60, -60)
  turtle.clearscreen()
  t = turtle.Turtle()
  t.speed("fastest")
  t.pensize(2)
  R = 90
  diam = 8
  
  # mark center
  t.dot(diam)
  t.up()
  
  # draw circle as reference frame (left of turtle)
  t.sety(-R)
  t.down()
  t.circle(R)
  t.up()
  
  def dot(x, y, color="black"):
    t.setx(x * R)
    t.sety(y * R)
    t.down()
    t.dot(diam, color)
    t.up()
  
  # mark the angles
  for point in points:
    dot(*point)
  
  dot(mx, my, "blue")
  dot(mx/unison, my/unison, "orange")
  dot(cos(angularAverage), sin(angularAverage), "green")
  
  t.hideturtle()

#--------------------------------------------------------------------------------------------------#

run()
print("Enter q to quit, anything else to rerun")
while input() != "q":
  run()
