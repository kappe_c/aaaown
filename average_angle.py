from math import pi as PI

import numpy as np


TWOPI = PI * 2.
RAD2DEG = 180. / PI

#--------------------------------------------------------------------------------------------------#

def average_angle_of_two(a, b, weights=None, wantSum=False):
  """ Like `average_angle` but just for two angles. """
  bIsLess = b < a
  d = a - b if bIsLess else b - a
  # See documentation. Uncomment for debugging
  #assert d < TWOPI, "Angles are %.2f radians apart" % d
  if d == PI:
    print("Warning: average angle of %.1f deg and %.1f deg is ambiguous" % (a*RAD2DEG, b*RAD2DEG))
  if d > PI:
    if bIsLess:
      b += TWOPI
    else: 
      a += TWOPI
  return np.average((a, b), None, weights, wantSum)

#--------------------------------------------------------------------------------------------------#

def average_angle(anglesInRad, weights=None, wantSum=False, isSorted=False):
  """ Compute the mean of a sequence of angles given in radians.
  
  This function considers the fact that the mean of two angles interpreted as positions on a circle
  can be the center of the smaller or larger arc connecting the points.
  We always choose the smaller one; e.g. mean(0, 182) is not 91 but 271 (or -89), and
  mean(10, 350) is not 180 but 0 (or 360).
  
  The input angles must come in a 1D array-like and must not cover more than two pi,
  the offset does not matter however (may be e.g. -pi or 0).
  The output may be in any range (use e.g. val % TWOPI to clamp your result).
  This function works for arrays of size 1 but not for empty arrays.  
  If `wantSum` is true, a pair `mean, sumOfWeights` is returned.
  
  If the input is already sorted ascendingly, `isSorted` can be set to True to increase performance.
  """
  
  numAngles = len(anglesInRad)
  if numAngles == 1:
    return (anglesInRad[0], weights[0]) if wantSum else anglesInRad[0]
  
  if numAngles == 2:
    return average_angle_of_two(anglesInRad[0], anglesInRad[1], weights, wantSum)
  
  # For efficiency and numerical stability, we try to use the normal method whenever possible.
  # This is possible if the angles are distributed along only half a circle.
  peakToPeak = anglesInRad[-1] - anglesInRad[0] if isSorted else np.ptp(anglesInRad)
  assert peakToPeak < TWOPI, "Angles are spread over %.1f degrees" % (peakToPeak*RAD2DEG) # see doc
  if peakToPeak <= PI:
    return np.average(anglesInRad, None, weights, wantSum)
  
  # Check if the numbers cover a range less than or equal pi starting at any of the angles and
  # wrapping around. To do this efficiently, we sort the angles (and the weights with them).
  #print("[average_angle] *sort & search gap* algorithm is used")

  # `sorted` returns a copy, which is good because then the input to this function is never altered
  if not isSorted:
    if weights is None:
      anglesInRad = sorted(anglesInRad)
    else:
      anglesInRad, weights = (list(t) for t in zip(*sorted(zip(anglesInRad, weights))))
  
  maxgap = 0.; spliti = 0 # may be used later
  for i in range(1, numAngles):
    # If there is a gap greater than pi anywhere, then the complement arc must be less than pi.
    gap = anglesInRad[i] - anglesInRad[i-1]
    if gap >= PI:
      # Add two pi to all angles before the gap.
      if isSorted:
        anglesInRad = np.copy(anglesInRad)
      # NOTE optimization possibility:
      # check which subset is larger and possibly subtract two pi from the second subset
      if isinstance(anglesInRad, np.ndarray):
        anglesInRad[:i] += TWOPI
      else:
        for j in range(i):
          anglesInRad[j] += TWOPI
      return np.average(anglesInRad, None, weights, wantSum)
    if gap > maxgap:
      maxgap = gap; spliti = i
  
  # We reuse the above idea for a divide-and-conquer algorithm:
  # 1. Split the input until the range is less than or equal pi.
  # 2. Merge partial results computing the mean of the means.
  #    Here, it is important to weight the partial results correctly,
  #    either by the size of the respective subarray or, if given, by the respective sum of weights.
  #print("[average_angle] *divide & conquer* algorithm is used for %d angles" % numAngles)
  #with np.printoptions(precision=1, suppress=True):
    #print(np.multiply(anglesInRad, RAD2DEG))
  
  # There are several splitting strategies imaginable, e.g. work-balancing.
  # We opt for a binary partition, splitting between the two angles with the largest difference.
  # Notice that actually at least two cuts should be made on the imaginary circle, but to use the
  # array structure efficiently, we always keep the one cut between 0 and two pi.
  
  # spliti is the first index of the second partition.
  # We know that there are at least three angles at this stage.
  
  m0 = None
  w0 = None
  if spliti == 1:
    m0 = anglesInRad[0]
    w0 = 1. if weights is None else weights[0]
  else:
    ws = None if weights is None else weights[:spliti]
    m0, w0 = average_angle(anglesInRad[:spliti], ws, True, True)
  
  m1 = None
  w1 = None
  if spliti == numAngles - 1:
    m1 = anglesInRad[-1]
    w1 = 1. if weights is None else weights[-1]
  else:
    ws = None if weights is None else weights[spliti:]
    m1, w1 = average_angle(anglesInRad[spliti:], ws, True, True)
  
  return average_angle_of_two(m0 % TWOPI, m1 % TWOPI, (w0, w1), wantSum)
