import numpy as np

from scipy.optimize import linear_sum_assignment



class PointTrackerComplex:
  """ Foremost to be used by PolylineFromMovingPoint. """
  
  def __init__(self, dist_func, maxAllowedDist=float('inf')):
    self._dist_func = dist_func
    assert maxAllowedDist > 0.
    self._maxAllowedDist = maxAllowedDist
    
    # mapping from globally consistent track index to list of globally unique point indices
    self._tracks = []
    # mapping from globally consistent track index to point for distance computation
    self._looseEnds = []
    # number of points so far; next globally unique point index
    self._numPts = 0
  
  
  def get_tracks(self):
    return self._tracks
  
  
  def first_time_step(self, initialPoints):
    for p, point in enumerate(initialPoints):
      self._tracks.append([p])
      self._looseEnds.append(point)
    self._numPts += len(initialPoints)
  
  
  
  def next_time_step(self, newPoints):
    tracks = self._tracks
    oldPoints = self._looseEnds
    dist_func = self._dist_func
    
    # We do not yet know how many of the new points we can match with an old one because
    # they may be forced to start a new track due to self._maxAllowedDist or too few old points.
    
    # In the following, p refers to an old point, q to a new point,
    # r to a new point we actually match with some p.
    
    numP = len(oldPoints)
    numQ = len(newPoints)
    matchingQs = set(range(numQ)) # as sorted list: r2q
    nonMatchingQs = []
    r2p2dist = []
    
    for q, newPoint in enumerate(newPoints):
      p2dist = np.empty(numP)
      # exclude new points, that can never be matched, as early as possible
      minDist = dist_func(newPoint, oldPoints[0])
      p2dist[0] = minDist
      
      for p in range(1, numP):
        dist = dist_func(newPoint, oldPoints[p])
        p2dist[p] = dist
        if dist < minDist:
          minDist = dist
      
      if minDist > self._maxAllowedDist:
        matchingQs.discard(q)
        nonMatchingQs.append(q)
      else:
        r2p2dist.append(p2dist)
    
    
    rs = None
    ps = None
    while r2p2dist:
      # This whole approach may be high quality but not very efficient.
      # That means both the single linear_sum_assignment and in particular looping over it.
      rs, ps = linear_sum_assignment(r2p2dist)
      # The arrays have size min(numP, numR).
      
      rsToDiscard = []
      for r, p in zip(rs, ps):
        if r2p2dist[r][p] > self._maxAllowedDist:
          rsToDiscard.append(r)
      
      if not rsToDiscard:
        break
      
      r2q = sorted(matchingQs)
      for r in rsToDiscard:
        q = r2q[r]
        matchingQs.discard(q)
        nonMatchingQs.append(q)
        del r2p2dist[r]
    
    
    # The rs are sorted ascendingly. Whenever there is a gap, we have a non-matching r
    # (because there have been less ps than rs).
    # (We can ignore the inverse case that there is an unmatched p -- it just stays a loose end.)
    possibleGapBeg = 0
    r2q = sorted(matchingQs)
    for r, p in zip(rs, ps):
      # get leading and intermediate gaps
      for unmatchedR in range(possibleGapBeg, r):
        #matchingQs.discard(q) # still necessary here?
        nonMatchingQs.append(r2q[unmatchedR])
      possibleGapBeg = r + 1
      
      q = r2q[r]
      tracks[p].append(self._numPts + q)
      oldPoints[p] = newPoints[q]
    
    # get trailing gaps
    numR = len(r2p2dist)
    for unmatchedR in range(possibleGapBeg, numR):
      #matchingQs.discard(q) # still necessary here?
      nonMatchingQs.append(r2q[unmatchedR])
      
    # start a new track for each unmatched new point
    for q in nonMatchingQs:
      tracks.append([self._numPts + q])
      oldPoints.append(newPoints[q])
    
    self._numPts += numQ
