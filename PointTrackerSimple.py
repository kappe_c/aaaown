import numpy as np

from scipy.optimize import linear_sum_assignment



class PointTrackerSimple:
	"""Made to match the interface of PointTrackerComplex"""
	def __init__(self, dist_func, maxAllowedDist=float('inf')):
		#print('A PointTrackerSimple is constructed')
		self._dist_func = dist_func
		self._t2local2point = None # collect the data the user provides
	
	
	def get_tracks(self):
		'''lists of globally unique point indices'''
		res = self.do_it()
		self._t2local2point = None
		return res
	
	
	def first_time_step(self, initialPoints):
		self._t2local2point = [initialPoints]
	
	
	def next_time_step(self, newPoints):
		self._t2local2point.append(newPoints)
	
	
	def do_it(self):
		t2local2point = self._t2local2point
		dist_func = self._dist_func
		
		# we will need the following mapping later
		# (the number of points per time step need not be constant)
		t2local2global = []
		numGlobal = 0
		for local2point in t2local2point:
			numLocal = len(local2point)
			t2local2global.append(np.arange(numGlobal, numGlobal+numLocal))
			numGlobal += numLocal
		
		# mapping from one global point index to next (if any), to construct the tracks later
		a2b = {}
		#numLocal0 = len(t2local2point[0])
		#globalToTrack = { g: [g] for g in range(numLocal0) }
		#globalsToContinue = set(range(numLocal0)
		
		for t in range(len(t2local2point)-1):
			# p refers to the local index of a point from t
			# q refers to the local index of a point from t+1
			p2point = t2local2point[t]
			q2point = t2local2point[t+1]
			
			p2q2dist = np.empty((len(p2point), len(q2point)))
			for q2dist, point in zip(p2q2dist, p2point):
				for q, qoint in enumerate(q2point):
					q2dist[q] = dist_func(qoint, point)
			
			# BEGIN core
			ps, qs = linear_sum_assignment(p2q2dist)
			# the arrays have size min(numP, numQ)
			# END core
			
			p2global = t2local2global[t]
			q2global = t2local2global[t+1]
			for p, q in zip(ps, qs):
				a2b[p2global[p]] = q2global[q]
		
		visited = set()
		head2track = {}
		#b2a = { b: a for a, b in a2b.items() }
		for a, b in a2b.items():
			if a in visited:
				continue
			visited.add(a)
			
			track = head2track[a] = [a, b]
			while b in a2b:
				if b in head2track: # this implies b in visited
					track.pop()
					track.extend(head2track[b])
					del head2track[b]
					break
				else:
					visited.add(b)
					b = a2b[b]
					track.append(b)
		
		return head2track.values()
	
