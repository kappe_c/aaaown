from math import pi as PI, cos

import numpy as np



def smooth_curves(iPoints, curves=None, bandwidth=3, kernel=None, average_func=None):
  """ Smooth a set of curves with a moving average.
  
  Parameters
  ----------
  iPoints : array-like, shape n or (n, k)
    Image of the regularly sampled 1D->kD functions (each column is one image dimension).
    Remains unchanged.
  curves : iterable of sequences, optional
    For each curve, the indices (at least `bandwidth`*2+1) into `iPoints`. Remains unchanged.
    By default, `iPoints` is interpreted as a single curve.
  bandwidth : int, optional
    Number of samples to consider on each side of the current one;
    `bandwidth`*2+1 is the window width (in number of samples).
  kernel : callable or string, optional
    Either a function that computes the weight of a sample within the window, or
    one of the following strings.
    NOTE use SciPy (?)
    NOTE actually currently ignored: the *full cosine* kernel ("cosful") is always used
  average_func : callable, optional
    Function to compute the average given a sequence of points and corresponding weights.
  
  Returns
  -------
  oPoints : ndarray, shape n or (n, k)
    The output points, making up the smoothed curves.
  """
  
  assert bandwidth >= 1
  # for the full cosine kernel: scale the default frequency (1/2pi) to 1/windowWidth
  # With the formula documented above, this implies the used bandwidth is actually 0.5 larger than
  # what the user has specified -- this is good because so the farthest included point still
  # has weight clearly greater than 0 while the next one is clearly outside the window.
  frequency = PI / (bandwidth + 0.5)
  # `diff` as in "I am the diff-nearest neighbor on one side where 0 is for the point itself"
  kernel = lambda diff: (cos(diff * frequency) + 1.)# / 2. # normalization is unnecessary
    
  # Because we always use the full window width (see below) and have a regular sampling,
  # we have the same sequence of weights all the time (think convolution).
  weights = np.fromiter((kernel(diff) for diff in range(-bandwidth, bandwidth+1)), "f8")
  
  if average_func is None:
    average_func = lambda ps, ws: np.average(ps, 0, ws)
  
  oPoints = np.empty_like(iPoints)
  
  if curves is None:
    curves = tuple(range(len(iPoints))), # 1-tuple (notice the comma) of a tuple
  
  for curve in curves: # each curve is a list of globally unique sample indices
    num = len(curve)
    bw = bandwidth
    ws = weights
    if num < bandwidth * 2 + 1:
      bw = (num - 1) // 2
      print("[smooth_curves] num. samples of curve (%d) is less than bandwidth*2+1 (%d) --> decreasing bandwidth from %d to %d for this curve" % \
        (num, bandwidth * 2 + 1, bandwidth, bw))
      ws = np.fromiter((kernel(diff) for diff in range(-bw, bw+1)), "f8")
    
    # get only the points on this curve (to make everything easier)
    iPts = None
    if isinstance(iPoints, np.ndarray) and not isinstance(curve, tuple):
      iPts = iPoints[curve] # this makes not only the right selection but also the right order
    else:
      iPts = [iPoints[glo] for glo in curve]
    
    # Regarding terminology, we assume the parametrization space (domain of the curve) is time,
    # so we call everything before the current output sample *past* and everything after *future*.
    
    # As is well known, the front and back of the curve are tricky to handle with a moving average.
    # There are several ways to deal with it, i.a.:
    # 1. Handle the ends like the rest.
    #    But this biases the front to the future and the back to the past.
    # 2. Crop a bandwidth wide part at the ends. But we do not like to shorten the range.
    # 3. Extend the input curve to the past and future by reflection at the end points.
    #    This has the effect that the ends are less smoothed (the first and last point actually
    #    remain unchanged because both sides of the kernel cancel each other out).
    # We opt for 3.
    
    oPts = np.empty_like(iPts)
    firstOrdinary = bw
    lastOrdinary = num - 1 - bw
    
    # middle (ordinary) part
    for i in range(firstOrdinary, lastOrdinary + 1):
      oPts[i] = average_func(iPts[i-bw : i+bw+1], ws)
    
    if bw == 1:
      oPts[0] = iPts[0]
      oPts[-1] = iPts[-1]
    else:
      # The formula to reflect (a scalar or vector) *a* at *p* is Ref_p(a) = 2p-a.
      # For the input extension, we disregard the boundary point (which we use as reflection point:
      # *odd* contrary to *even* method) and we exclude the point *bandwidth* away from the boundary
      # (would only contribute to the first/last output and there cancels out).
      lenOfExt = bw - 1
      
      # front part
      extension = iPts[0]*2 - iPts[lenOfExt : 0 : -1]
      oPts[0] = iPts[0]
      for i in range(1, firstOrdinary):
        minj = i - bw # minj < 0
        maxj = i + bw
        myPts = np.concatenate((extension[minj:], iPts[: maxj+1]))
        #assert len(myPts) == bw * 2 + 1
        oPts[i] = average_func(myPts, ws)
      
      # back part
      extension = iPts[-1]*2 - iPts[-2 : -2-lenOfExt : -1]
      for i in range(lastOrdinary + 1, num - 1):
        minj = i - bw
        maxj = i + bw # maxj >= num
        myPts = np.concatenate((iPts[minj:], extension[:maxj-num+1]))
        #assert len(myPts) == bw * 2 + 1
        oPts[i] = average_func(myPts, ws)
      oPts[-1] = iPts[-1]
    
    # Write the result to the global output
    for loc, glo in enumerate(curve):
      oPoints[glo] = oPts[loc]
    
  return oPoints
