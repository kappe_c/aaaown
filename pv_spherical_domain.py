from math import sqrt, sin, cos, atan2, asin, acos

import numpy as np
from numpy.linalg import norm as np_linalg_norm

from vtkmodules.numpy_interface import dataset_adapter as dsa



def get_radius(dataset):
  res = None
  if type(dataset.FieldData.GetArray("radius")) != dsa.VTKNoneArray:
    res = dataset.FieldData["radius"]
  else:
    # assuming a constant radius we just look at the first point
    res = np_linalg_norm(dataset.Points[0])
  #print("Radius: %f" % res)
  #tol = res * 1e-6 # NOTE only for testing
  #for i, p in enumerate(dataset.Points):
    #if abs(np_linalg_norm(p) - res) > tol:
      #print("!!!! p%d has norm %.2f," % (i, np_linalg_norm(p)), p)
  return res

#--------------------------------------------------------------------------------------------------#

def cartesian_to_spherical(iPoints):
  oPoints = np.empty((len(iPoints), 2), "f8")
  for iPoint, oPoint in zip(iPoints, oPoints):
    x = iPoint[0]
    y = iPoint[1]
    z = iPoint[2]
    r = sqrt(x*x + y*y + z*z)
    oPoint[0] = atan2(y, x) # Python atan2 is in (-pi, pi] (does not matter)
    oPoint[1] = asin(z / r) # Python asin is in [-pi/2, pi/2] => equator at 0 => fits
  return oPoints



def spherical_to_cartesian(iPoints, r):
  oPoints = np.empty((len(iPoints), 3), "f8")
  for iPoint, oPoint in zip(iPoints, oPoints):
    lon = iPoint[0]
    lat = iPoint[1]
    # lat in   [-pi/2, 0, pi/2] =>
    # sin(lat) in [-1, 0, 1] for z
    # cos(lat) in  [0, 1, 0] to scale x and y
    cosLatRadius = cos(lat) * r
    oPoint[0] = cos(lon) * cosLatRadius
    oPoint[1] = sin(lon) * cosLatRadius
    oPoint[2] = sin(lat) * r
  return oPoints

#--------------------------------------------------------------------------------------------------#

def angle_between_two_points(p, q):
  """ Angle (in radian) between two 3d (Cartesian) points (none may be the zero vector) """
  return acos(np.dot(p, q) / (np_linalg_norm(p) * np_linalg_norm(q)))
